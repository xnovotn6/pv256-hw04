package com.example.pv256_hw04.viewModels

import androidx.lifecycle.ViewModel
import com.example.pv256_hw04.models.Score
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow

// GameViewModel is a ViewModel class that is used to store and manage the scores of two teams.
class GameViewModel : ViewModel() {
    // The scores of the two teams are stored in MutableStateFlow objects.
    // MutableStateFlow is a type of Flow that represents a mutable value, similar to LiveData.
    // The scores are private and are exposed as StateFlow objects.
    private val _scoreTeamA = MutableStateFlow(0)
    // scoreTeamA is a StateFlow that represents a read-only state.
    val scoreTeamA: StateFlow<Int> = _scoreTeamA

    private val _scoreTeamB = MutableStateFlow(0)
    val scoreTeamB: StateFlow<Int> = _scoreTeamB

    // The incrementScoreTeamA and incrementScoreTeamB methods are used to increment the scores of
    // the two teams.
    fun incrementScoreTeamA(score: Score) {
        _scoreTeamA.value += score.value
    }

    fun incrementScoreTeamB(score: Score) {
        _scoreTeamB.value += score.value
    }

    // The resetScores method is used to reset the scores of the two teams to 0.
    fun resetScores() {
        _scoreTeamA.value = 0
        _scoreTeamB.value = 0
    }
}
