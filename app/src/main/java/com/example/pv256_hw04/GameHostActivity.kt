package com.example.pv256_hw04

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleEventObserver
import androidx.lifecycle.LifecycleOwner
import com.example.pv256_hw04.ui.GameFragment

class GameHostActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game)

        // Not part of this seminar (yet)
        // --- Nerds only section ---
        // Android Developer Guide: https://developer.android.com/guide/components/fragments
        // This is fragment. A fragment is a reusable class implementing a portion of an activity.
        // It is a part of the activity's user interface and contributes its own layout and behavior
        // to the activity. A fragment can be used in multiple activities. The original purpose of
        // fragments was to support large screens, such as tablets, by allowing the same fragment to
        // be used in multiple activities. However, fragments are now used to support a wide variety
        // of screen sizes and device orientations.
        //
        // This is archaic way of adding/navigating a fragment to an activity. Today, it is
        // recommended to use Navigation Component: https://developer.android.com/guide/navigation
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .add(R.id.game_fragment, GameFragment.newInstance())
                .commit()
        }
    }
}