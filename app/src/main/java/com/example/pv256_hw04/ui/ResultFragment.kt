package com.example.pv256_hw04.ui

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.pv256_hw04.databinding.FragmentResultBinding
import com.example.pv256_hw04.viewModels.GameViewModel

class ResultFragment : Fragment() {

    // TODO #9: Create a GameViewModel instance, shared by the GameFragment and ResultFragment.
    //  Be aware in which lifecycle scope you should create the ViewModel instance. Otherwise, you
    //  spend a lot of time debugging why app crashes.
    // TODO #10: Implement/initialize view binding
    private lateinit var viewModel: GameViewModel

    private var _binding: FragmentResultBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        bindViews()
        return binding.root
    }

    @SuppressLint("QueryPermissionsNeeded")
    private fun bindViews() {
        // TODO #11: Get the scores from the ViewModel and update the UI. You don't need to observe
        //  the scores. Can you think of a reason why? (don't overthink answer)

        // TODO #12: Set the click listener for the "Share" button. When the button is clicked, create
        //  an implicit intent to share the scores of the two teams.
        //  Subject: "Score"
        //  Text: "Team A: <score of team A>\nTeam B: <score of team B>"
        //  Android Developer documentation: https://developer.android.com/training/sharing/send

    }

    companion object {
        @JvmStatic
        fun newInstance() = ResultFragment()
    }
}