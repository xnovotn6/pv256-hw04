package com.example.pv256_hw04.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.example.pv256_hw04.R
import com.example.pv256_hw04.models.Score
import com.example.pv256_hw04.databinding.FragmentGameBinding
import com.example.pv256_hw04.viewModels.GameViewModel
import kotlinx.coroutines.launch

class GameFragment : Fragment() {
    // TODO #4: Create a GameViewModel instance, shared by the GameFragment and ResultFragment.
    //  Be aware in which lifecycle scope you should create the ViewModel instance. Otherwise, you
    //  spend a lot of time debugging why app crashes.
    // TODO #5: Implement/initialize view binding

    private lateinit var viewModel: GameViewModel

    private var _binding: FragmentGameBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        bindViews()
        return binding.root
    }

    private fun bindViews() {
        // TODO #6: Set the click listeners for the buttons to increment the scores of the two teams

        // TODO #7: Set the click listener for the "End Game" button. When the button is clicked,
        //  navigate to the ResultFragment. You can inspire yourself in the GameHostActivity.kt file.

        // This is coroutine. A coroutine is an instance of suspendable computation. It is similar
        // to a thread in a way that it takes a block of code to run **concurrently** ("parallel").
        // However, a coroutine is not bound to any particular thread. It may suspend its execution
        // in one thread and resume in another one.
        lifecycleScope.launch {
            // ViewModel contains scoreTeamA (and scoreTeamB) which is a StateFlow. StateFlow is
            // **observable** - you can observer scoreTeamA for changes and when value changes, UI
            // will be updated automatically (massively oversimplified theory, but that's all you
            // need to know).
            viewModel.scoreTeamA.collect { score ->
                binding.teamAScore.text = score.toString()
            }
        }

        // TODO #8: Observe the scores of the two teams and update the UI accordingly for Team B.
        //  You can inspire yourself using the code from the function above.
        lifecycleScope.launch {
            viewModel.scoreTeamB.collect { score ->
                binding.teamBScore.text = score.toString()
            }
        }
    }

    companion object {
        @JvmStatic
        fun newInstance() = GameFragment()
    }
}