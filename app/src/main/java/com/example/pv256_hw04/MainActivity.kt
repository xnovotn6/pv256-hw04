package com.example.pv256_hw04

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity

// TODO #0: Log all lifecycle events of the MainActivity. You can use lifecycle callbacks or
//  LifecycleObserver.
class MainActivity : AppCompatActivity() {
    // TODO #1: Implement/initialize view binding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // TODO #2: Use view binding to set the content view
        setContentView(R.layout.activity_main)

        // TODO #3: Set the click listener for the "Start Game" button. When the button is clicked,
        //  start the GameHostActivity (hint: explicit intent)
        //  Android Developer documentation: https://developer.android.com/guide/components/intents-filters
    }
}