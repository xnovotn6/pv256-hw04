package com.example.pv256_hw04.models

enum class Score(val value: Int) {
    One(1), Two(2), Three(3),
}

